# Inherit some common lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_tablet_wifionly.mk)

# Inherit device configuration for icosa_sr.
include device/nvidia/foster/lineage.mk
TARGET_INIT_VENDOR_LIB := //device/nintendo/icosa_sr:libinit_icosa_sr
$(call inherit-product, device/nintendo/icosa_sr/full_icosa_sr.mk)

# Include Maru stuff
$(call inherit-product, vendor/maruos/device-maru.mk)
$(call inherit-product, vendor/maruos/BoardConfigVendor.mk)

PRODUCT_NAME := maru_icosa_sr
PRODUCT_DEVICE := icosa_sr
MARU_VERSION := 0.8
MARU_BUILD_VERSION := $(MARU_VERSION)-$(shell date -u +%Y%m%d)
